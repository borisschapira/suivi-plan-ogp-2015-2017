# Engagement 10 : Donner aux citoyens de nouveaux moyens de participer à la vie publique en les associant à l'identification de problèmes à résoudre

## Institutions porteuses :
- Premier ministre
- Secrétariat d'État chargé du Numérique
- Secrétariat d'État chargé de la Ville

## Enjeux : 
Au-delà de la consultation des citoyens, le gouvernement ouvert s'appuie aussi sur d'autres
formes de participation : **des plateformes qui permettent aux citoyens de signaler des
problèmes à résoudre**, des incidents ou de contribuer à la connaissance de certains
phénomènes, au travers de contributions ponctuelles. Surtout, ces plateformes conduisent le
gouvernement à s'emparer de problèmes effectivement constatés, vérifiés et signalés par les
citoyens.

Ces formes de production participative (crowdsourcing) renvoient à des enjeux très
concrets : signalement des problèmes et incidents sur la voie publique, vigilance sanitaire
grâce notamment à la possibilité de signaler les effets indésirables de tous les médicaments
sur le site [medicaments.gouv.fr](http://www.medicaments.gouv.fr/) et bientôt de l'ensemble des produits et activités de santé,
pratique de sciences participatives (projet [SPIPOLL](http://www.spipoll.org/)…), développement de « capteurs
citoyens » pour partager les mesures environnementales…

## Description de l'engagement : 

- **Lancer l'appel à projets « Fix-it des quartiers » fin 2015**
    * Mettre à disposition une première version de la solution numérique de signalement d'incidents en septembre 2015 (objectif : 100 communes concernées avant la fin de l'année)
    * Lancer mi-2016 les solutions pouvant être généralisées

- **Dans l'espace numérique, selon la logique de la démarche « Fix-it des quartiers », il sera offert aux citoyens la possibilité de contribuer à l'identification d'incidents (problèmes de sécurité ou défigurations de sites internet, notamment) et leur signalement.**
    * Mettre en place de manière expérimentale une plateforme de signalement "Fix-it numérique" courant 2016. L'ouvrir au public fin 2016.

[En savoir plus](http://gouvernement-ouvert.etalab.gouv.fr/content/fr/consulter-concerter-co-produire/action-publique/engagement-10.html)

### Description des résultats :

**Actions** | **Résultats** | **Prochaines étapes** | **Statut**
--- | --- | --- | ---
Lancer l'appel à projets « Fix-it des quartiers » fin 2015. |  Le secrétariat d’Etat chargé du Numérique ne considère plus ce projet comme opportun puisqu’au moins trois applications déjà existantes sont déjà très efficaces (voir [DansMaRue]( http://www.frenchweb.fr/bon-app-dansmarue-lapp-pour-signaler-une-anomalie-a-la-mairie-de-paris/161236), [PopVox]( https://www.popvox.fr/) ou en anglais [AppMyCity]( http://www.appmycity.org/)). Cet engagement devrait devenir un encouragement de soutien aux innovations dans ce domaine et un encouragement aux collectivités à adopter l'une des solutions déjà sur le marché. La préfecture du Val d'Oise a notamment mis en place une application appelée ["Ma Préfecture"](http://www.val-doise.gouv.fr/Publications/Application-Mobile-MaPrefecture/Lancement/Bernard-CAZENEUVE-lance-officiellement-l-application-mobile-Ma-Prefecture) inaugurée le 11 mai 2016 par le Ministre de l'Intérieur. | Proposer une alternative à cet engagement avant l’été. Vérifier si l'application du Val d'Oise est open source et si elle peut être réutilisée (au moins par d'autres préfectures). | Non démarré
Mettre en place de manière expérimentale une plateforme de signalement courant 2016. | Retour nécessaire de l'ANSSI. Relance réalisée début mai 2016. | | Non démarré



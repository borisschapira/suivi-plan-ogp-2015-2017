# Engagement 4 : Ouvrir l'accès aux évaluations de politiques publiques et à leurs conclusions

## Institutions porteuses : 
Secrétariat d'Etat chargé de la Réforme de l'Etat et de la Simplification

## Enjeux : 
**L'évaluation des politiques publiques aide les décideurs à rendre l'action publique plus pertinente, efficace et efficiente, et contribue à l'information du citoyen**. Les enjeux de l'évaluation dépassent ceux de ses protagonistes directs et concernent l'ensemble des citoyens. Toutefois, l'accès à ces différents travaux reste complexe du fait du grand nombre d'acteurs impliqués et de la pluralité des supports de diffusion retenus.

## Description de l'engagement : 
- **Encourager l'ensemble des acteurs de l'évaluation à mettre à disposition leurs travaux dans l'observatoire de l'évaluation des politiques publiques pour améliorer son exhaustivité, faciliter les recherches dans l'observatoire (accès par mot clé, etc.)**
    - Rendre accessible à tous l'Observatoire de l'évaluation des politiques publiques, base documentaire de référencement des évaluations de politiques publiques
    - Systématiser la publication des évaluations de politiques publiques lancées dans le cadre de la modernisation de l'action publique (hors empêchement juridique)
- **Systématiser et enrichir la participation citoyenne dans les évaluations coordonnées par le SGMAP, par exemple à travers des enquêtes d'opinion, des ateliers de travail, des jurys citoyens, et d'autres formes de participation**
- **Améliorer la traçabilité de l'impact sur l'action publique des évaluations de politiques publiques lancées dans le cadre de la modernisation de l'action publique**

[En savoir plus](http://gouvernement-ouvert.etalab.gouv.fr/content/fr/rendre-des-comptes/evaluation-publique/engagement-4.html)
 
## Description des résultats :

**Actions** | **Résultats** | **Prochaines étapes** | **Statut**
--- | --- | --- | ---
Encourager l'ensemble des acteurs de l'évaluation à mettre à disposition leurs travaux dans l'observatoire de l'évaluation des politiques publiques pour améliorer son exhaustivité, faciliter les recherches dans l'observatoire (accès par mot clé, etc.). Rendre accessible à tous l’Observatoire de l’évaluation des politiques publiques, base documentaire de référencement des évaluations de politiques publiques | L'[observatoire de l'évaluation des politiques publiques (OEPP)](http://ww5.eudonet.com/V7/app/specif/EUDO_03847/ExtranetDocumentaire/Recherche.aspx) recense aujourd'hui 1942 documents, accessibles par 6 clés de tri possibles (intitulé, année, thématique, échelle territoriale, territoire, commanditaire). L'OEPP est accessible à tous et gratuitement.| | Substantiel
Systématiser la publication des évaluations de politiques publiques lancées dans le cadre de la modernisation de l’action publique (hors empêchement juridique). | [Un espace en ligne dédié au suivi des évaluations des politiques publiques](http://modernisation.gouv.fr/laction-publique-se-transforme/en-evaluant-ses-politiques-publiques/toutes-les-evaluations-de-politiques-publiques) a été créé. Cet espace permet de suivre l'état d'avancement de toutes les évaluations ainsi que tous les documents qui leurs sont rattachés (lettre de mission, fiche de cadrage, rapports d'évaluation - 51 rapports ont été d'ores et déjà publiés - , rapports de consultation des usagers). | Augmenter la proportion de documents téléchargeables sur l'espace en ligne et les statistiques de fréquentation du site . Définir une métrique sur les documents et leur consultation en ligne.| Partiel
Systématiser et enrichir la participation citoyenne dans les évaluations coordonnées par le SGMAP, par exemple à travers des enquêtes d’opinion, des ateliers de travail, des jurys citoyens, et d’autres formes de participation. | Toutes les évaluations de politiques publiques sont désormais examinées sous l'angle de la participation citoyenne et les ministères commanditaires sont systématiquement sensibilisés sur ce point ; des consultations sont lancées chaque fois que leur apport attendu le justifie. Des ateliers citoyens sont proposés (voir [engagement 13](Engagement13.md)).| Mettre en place de nouveaux dispositifs de participation citoyenne (ateliers citoyens) et élargir cette participation à la co-construction de scénarios (proposition d'une expérimentation avec le CESE). | Partiel
Améliorer la traçabilité de l’impact sur l’action publique des évaluations de politiques publiques lancées dans le cadre de la modernisation de l’action publique. | En cours d'expérimentation avec quelques ministères. | Poursuivre et élargir l'expérimentation en cours sur l'impact des évaluations de politiques publiques. | Partiel 


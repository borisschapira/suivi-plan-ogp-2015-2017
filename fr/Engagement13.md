# Engagement 13 : Capitaliser sur les consultations menées et rénover les dispositifs d'expression citoyenne

## Institutions porteuses : 
- Premier ministre
- Ministère de l'Environnement, de l'Energie et de la Mer
- Secrétariat d'État chargé de la Réforme de l'État et de la Simplification
- Commission nationale du débat public

## Enjeux : 
**La participation éclairée des citoyens au processus de décision publique** repose sur la capacité des administrations à faciliter la compréhension par chacun d'entre eux du fonctionnement des institutions et de leur activité, à leur ouvrir des ressources activables et à mobiliser efficacement leur contribution.

Comme le souligne France Stratégie dans son rapport sur [L'action publique de demain](http://www.strategie.gouv.fr/publications/action-publique-demain-5-objectifs-5-leviers) : « _la société aspire à ce que des pouvoirs réels soient attribués aux instances citoyennes, en complément de la démocratie représentative_ ». Les dispositifs participatifs se sont multipliés, sous une large diversité de formats. « _Bien conduites, ces démarches contribuent à retisser la confiance au sein de la population car elles incarnent le principe d'interpellation des citoyens_ ».

Les acteurs publics doivent ainsi pouvoir capitaliser sur les consultations déjà menées, mobiliser aisément les dispositifs de dialogue et de concertation, et intégrer efficacement ces apports au processus d'élaboration de la décision publique.  

Par ailleurs, les citoyens attendent, en contrepartie de leur investissement, une plus grande ouverture des consultations à un public renouvelé, des termes du débat clairement exposés et une information claire et transparente sur les règles de la concertation, en particulier sur l'utilisation et les suites données à leurs contributions.


## Description de l'engagement : 
- **Capitaliser sur l'historique des consultations : enrichir le recensement et renforcer l'accessibilité des débats publics ayant eu lieu en France**
    - Poursuivre l'effort de recensement des débats publics en France en déployant plus largement sur le territoire la norme « DebatesCore » pour faciliter le recueil et l'accessibilité en un point d'accès unique des consultations menées
- **Outiller les acteurs publics pour réussir les consultations citoyennes**
    - Le secrétariat général pour la modernisation de l'action publique s'engage à proposer un dispositif simple et agile de consultation sous forme d' « Ateliers citoyens »
        - Expérimenter avec des ministères pilotes ce dispositif plus souple et plus agile que les formats classiques de consultation
        - Présenter aux administrations partenaires la charte établissant les grands principes des « Ateliers citoyens » ainsi que leur protocole de mise en œuvre
        - Accompagner les administrations partenaires dans la mise en place de ces ateliers
    - Le ministère de l'Environnement, de l'Énergie et de la Mer s'engage à proposer une « boîte à outils » pour guider les institutions dans le choix et la mise en place de dispositifs participatifs


[En savoir plus](http://gouvernement-ouvert.etalab.gouv.fr/content/fr/consulter-concerter-co-produire/renovation-des-pratiques/engagement-13.html)
 
## Description des résultats :

**Actions** | **Résultats** | **Prochaines étapes** | **Statut**
--- | --- | --- | ---
Capitaliser sur l’historique des consultations : enrichir le recensement et renforcer l’accessibilité des débats publics ayant eu lieu en France via la norme Debatescore. |  La DILA recense les consultations publiques sur [vie-publique.fr/forums](http://www.vie-publique.fr/forums/). La [norme DebatesCore](http://www.vie-publique.fr/forums/debatescore/debatescore-3.html) n'a pas été implémentée à la Commission nationale du débat public (CNDP). En revanche, sur la capitalisation de l'historique des consultations de la CNDP, le nouveau site de la CNDP permet un archivage global des dossiers de la CNDP en ligne. | Définir un mode de partenariat entre la DILA et le CNDP pour articuler le recensement des consultations et débats publics et leur animation, et imaginer des modalités d'ouverture des données. Réfléchir à une manière de valoriser la norme lors du Sommet (par exemple dans le cadre de la [boîte à outils du PGO]( https://www.etalab.gouv.fr/contribuez-a-la-boite-a-outils-pour-un-gouvernement-ouvert)). | Partiel
Outiller les acteurs publics pour réussir les consultations citoyennes : ateliers citoyens du SGMAP.| Avec les ateliers citoyens, le SGMAP lance une nouvelle forme de participation des citoyens à la décision publique. La signature le 9 février 2016 d’un partenariat avec la Commission nationale du débat public (CNDP) a donné le coup d’envoi. Les ateliers citoyens réunissent un panel restreint de citoyens représentatifs de la diversité sociodémographique et formés à la problématique afin de se forger un avis éclairé. Ils permettent de travailler en amont d'une consultation sur un sujet délicat ([en savoir plus](http://modernisation.gouv.fr/les-services-publics-se-simplifient-et-innovent/par-la-consultation-et-la-concertation/democratie-participative-des-ateliers-citoyens-pour-renforcer-la-participation-citoyenne)). Le Ministère de la Santé est le premier commanditaire : une douzaine de citoyens sont réunis pour émettre un avis sur le partage des données de santé, éclairé par des présentations, des cours en ligne, des séances de dialogue interactif en ligne avec les animateurs. Deux sessions en présentiel sont prévues (la première session du panel de citoyens a eu lieu le 21 mai). La [consultation en ligne sur le big data en santé]( http://www.faire-simple.gouv.fr/bigdatasante) rassemble déjà plus de 250 propositions. De nouveaux ministères candidats se sont fait connaître pour se lancer dans les ateliers citoyens. | Suivre le déroulé du premier atelier et l'extension à d'autres sujets et dans d'autres ministères intéressés. Tirer les conclusions de l'atelier santé à partir de fin juin. Outiller les consultations citoyennes grâce à la [boîte à outils du PGO]( https://www.etalab.gouv.fr/contribuez-a-la-boite-a-outils-pour-un-gouvernement-ouvert). | Substantiel

La boîte à outils des dispositifs participatifs est l'ambition du Sommet mondial du PGO en décembre 2016.

# Engagement 24 : Associer la société civile à la Conférence COP21 et favoriser la transparence sur l'agenda et les négociations

## Institutions porteuses : 
- Ministère des Affaires étrangères et du Développement international
- Ministère de l'Environnement, de l'Énergie et de la Mer
- Commission nationale du débat public

## Enjeux : 
La présidence française de la COP21 visera l'exemplarité dans l'écoute de la société civile. La forte mobilisation des citoyens et des acteurs non-étatiques (collectivités territoriales, on déroulé est un corollaire de l'engagement de la société civile.

## Description de l'engagement : 

- **Réunir les représentants de la société civile avant chaque réunion informelle de négociation**
    * Une première rencontre a été organisée à la veille de la session informelle sur les négociations sur le climat du 6-8 mai 2015, avec la société civile : ONG françaises et internationales, représentants à l'ONU des syndicats… afin d'y présenter les travaux et l'état d'avancement des négociations et d'engager la discussion
    * De nouvelles rencontres auront lieu en marge des prochaines sessions de négociations
- **Réaliser une plateforme participative pour mobiliser la société civile en préparation de la COP 21, qui pourra à terme être étendue à d'autres consultations**
    * Entre juin et novembre 2015, élaboration d'une première version, avec pour objectifs de :
        * sensibiliser au dialogue environnemental et aux grands enjeux de la transition énergétique ;
        * mettre en réseau les habitants, collectifs, porteurs de projets, entreprises, collectivités locales et parties prenantes ;
        * faciliter les prises de contact et la coopération sur le moyen terme entre les différents acteurs de la « citoyenneté environnementale », en ligne, notamment ;
        * créer des coopérations décentralisées et une communauté d'acteurs de l'e-citoyenneté environnementale ;
        * recueillir les suggestions et avis des internautes pour permettre le recensement collaboratif des initiatives locales et constituer une large base de données.
    * Développement d'une deuxième version de la plateforme afin de faire vivre la mobilisation citoyenne au-delà de COP 21. Elle pourrait offrir un support aux consultations citoyennes du Ministère de l'écologie et du développement durable et de l'énergie
- **Poursuivre la consultation sur les enjeux du climat afin de donner suite au débat citoyen planétaire du 6 juin 2015, qui a rassemblé plus de 10 000 citoyens de 75 pays**

[En savoir plus](http://gouvernement-ouvert.etalab.gouv.fr/content/fr/climat/engagement-24.html)
 
## Description des résultats :

**Actions** | **Résultats** | **Prochaines étapes** | **Statut**
--- | --- | --- | ---
Réunir les représentants de la société civile avant chaque réunion informelle de négociation. | En savoir plus sur le [site de la COP21](http://www.cop21.gouv.fr/)  | | Complet
Réaliser une plateforme participative pour mobiliser la société civile en préparation de la COP 21, qui pourra à terme être étendue à d'autres consultations. | Une plateforme participative a été lancée en prévision de la COP21 : [VotreEnergiePourLaFrance.fr](http://votreenergiepourlafrance.fr/). D'autres consultations ont eu lieu : le sénateur écologiste Joël Labbé, avec l'accord du rapporteur Jérôme Bignon, a soumis le [projet de loi pour la reconquête de la biodiversité, de la nature et des paysages](https://www.legifrance.gouv.fr/affichLoiPreparation.do?idDocument=JORFDOLE000028780525&type=general&typeLoi=proj&legislature=14) à consultation avant son passage en séance au Sénat. La consultation a lieu sur la plateforme Parlement & Citoyens. Cette initiative était soutenue par la Ministre.  | Détailler les résultats de l'initiative [VotreEnergiePourLaFrance.fr](http://votreenergiepourlafrance.fr/). | Complet
Poursuivre la consultation sur les enjeux du climat afin de donner suite au débat citoyen planétaire du 6 juin 2015, qui a rassemblé plus de 10 000 citoyens de 75 pays. | | |



# Engagement 23 : Responsabiliser et protéger les agents publics en matière de prévention des conflits d'intérêts

## Institutions porteuses : 
- Ministère de la Fonction publique

## Enjeux : 
La consécration de l'exemplarité de chaque fonctionnaire, dans son comportement et son action quotidienne au service de l'intérêt général, sont une partie intégrante du modèle républicain de la France. La transposition des mesures relatives à la transparence de la vie publique aux agents publics renforcera l'exemplarité de la fonction publique et les obligations du statut général. Elle s'accompagnera de l'introduction d'un dispositif de protection permettant à un agent de bonne foi de signaler l'existence d'un conflit d'intérêt sans crainte d'éventuelles pressions.

## Description de l'engagement : 
- **Désigner un fonctionnaire, un service ou une personne morale de droit public chargé d'apporter aux agents placés sous son autorité tout conseil utile au respect des obligations et des principes déontologiques**
- **Introduire des dispositions légales pour mieux prévenir les conflits d'intérêts et protéger les fonctionnaires**

[En savoir plus](http://gouvernement-ouvert.etalab.gouv.fr/content/fr/ouvrir-l-administration/deontologie/engagement-23.html)
 
## Description des résultats :

**Actions** | **Résultats** | **Prochaines étapes** | **Statut**
--- | --- | --- | ---
Désigner un fonctionnaire, un service ou une personne morale de droit public chargé d’apporter aux agents placés sous son autorité tout conseil utile au respect des obligations et des principes déontologiques. | La [loi relative à la déontologie et aux droits et obligations des fonctionnaires](https://www.legifrance.gouv.fr/affichLoiPreparation.do?idDocument=JORFDOLE000027721584&type=general) du 20 avril 2016 crée un référent déontologue dans les ministères, chargé d'apporter aux agents publics des conseils utiles au respect des obligations et principes déontologiques. | Suivre les décrets d'application de la loi et la création des référents déontologues. | Substantiel
Introduire des dispositions légales pour mieux prévenir les conflits d’intérêts et protéger les fonctionnaires : mettre en place un régime de déclaration d’intérêts à la charge des agents occupant certaines fonctions ; renforcer les pouvoirs et le champ de compétence de la commission de déontologie, qui seront étendus à la prévention des conflits d’intérêt et renforcés en matière de contrôle des départs vers le secteur privé ; introduire un régime de protection dans le statut général des fonctionnaires afin de permettre à un agent de bonne foi de signaler l’existence d’un conflit d’intérêt sans crainte d’éventuelles pressions. | La [loi relative à la déontologie et aux droits et obligations des fonctionnaires](https://www.legifrance.gouv.fr/affichLoiPreparation.do?idDocument=JORFDOLE000027721584&type=general) du 20 avril 2016 étend les modalités de protection des lanceurs d'alerte : les agents publics peuvent désormais alerter sur des risques de conflits d'intérêts, la rémunération et l'évaluation des agents s'ajoutent aux éléments ne pouvant être modifiés à l'issue d'une alerte de bonne foi. Elle instaure l'obligation pour certains emplois et fonctionnaires de réaliser une déclaration préalable d'intérêts et/ou de patrimoine. Elle renforce les pouvoirs et le champ de compérences de la Commission de déontologie de la fonction publique (avis qui peuvent être rendus publics, échanges d'informations possibles avec la Haute Autorité pour la transparence de la vie publique). | Suivre les décrets d'application de la loi. | Substantiel

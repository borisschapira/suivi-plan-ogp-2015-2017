# Engagement 25 : Mettre à disposition des données et des modèles relatifs au climat et au développement durable

## Institutions porteuses : 
- Ministère de l'Environnement, de l'Energie et de la Mer
- Secrétariat d'Etat chargé de la Réforme de l'Etat et de la Simplification

## Enjeux : 
L’ouverture et la mise en commun de données et modèles relatifs au climat et au développement durable servira à :
- Éduquer et sensibiliser les citoyens sur les enjeux du climat, avec des « datavisualisations » qui permettront d’illustrer les grands défis du climat (utilisation par les journalistes…) ;
- Outiller les argumentaires et les prises de positions des représentants de la société civile ;
- Stimuler l’innovation économique et sociale et permettre à des acteurs tiers de proposer des solutions innovantes aux défis du climat.

## Description de l'engagement : 
- **Ouvrir et mettre à disposition en format ouvert sur la plateforme data.gouv.fr des données, modèles et simulateurs relatifs au climat, à la transition énergétique, au développement durable**
- **Publier les données issues des études d’impacts réalisées par le ministère de l’Environnement, de l'Energie et de la Mer**

[En savoir plus](http://gouvernement-ouvert.etalab.gouv.fr/content/fr/climat/engagement-25.html)
 
## Description des résultats :

**Actions** | **Résultats** | **Prochaines étapes** | **Statut**
--- | --- | --- | ---
Ouvrir et mettre à disposition en format ouvert sur la plateforme data.gouv.fr des données, modèles et simulateurs relatifs au climat, à la transition énergétique, au développement durable. | Plus de 500 jeux de données nationaux et locaux relatifs au climat, à la transition énergétique et au développement durable ont été ouverts sur [data.gouv.fr](https://www.data.gouv.fr/fr/). Météo France a ouvert ses modèles et simulateurs sur [data.gouv.fr](https://www.data.gouv.fr/fr/) et sur sa propre plateforme [donneespubliques.meteofrance.fr](https://donneespubliques.meteofrance.fr/). Ces données ont particulièrement été mises en avant lors du [Climate Change Challenge](http://c3challenge.com/) (voir notamment les données portant le [badge C3](https://www.data.gouv.fr/fr/datasets/?badge=c3)) et des divers événements et hackathons dont Etalab était partenaire. | Ouvrir des données sur la qualité de l'air et améliorer la granularité des données de qualité de l'eau. | Complet
Publier les données issues des études d’impacts réalisées par le ministère de l’Environnement, de l'Energie et de la Mer. | L'article 3 ter du [projet de loi pour la reconquête de la biodiversité, de la nature et des paysages](https://www.legifrance.gouv.fr/affichLoiPreparation.do?idDocument=JORFDOLE000028780525&type=general&typeLoi=proj&legislature=14) prévoit que les données brutes des études d'impact soient reversées à l'Inventaire national du patrimoine naturel géré par le Muséum d'histoire naturelle et diffusées comme des données publiques, gratuites, librement réutilisables. Le ministère a engagé le projet de réalisation en méthode "design de services" afin d'associer les différents utilisateurs à la conception du produit.Le ministère de l'Environnement, de l'Energie et de la Mer a organisé du 4 au 5 juin un [hackathon sur les données de la biodiversité](http://www.developpement-durable.gouv.fr/Venez-participer-au-premier.html). | Suivre l'adoption du projet de loi et ses décrets d'application. Préciser la mise en oeuvre concrète de l'engagement d'ici juin 2017. Le ministère de l'Environnement, de l'Energie et de la Mer produira un prototype de la plate-forme études d'impact à l'automne 2016. Suivre la réalisation. | Substantiel

## Une belle histoire : 

- A l'occasion de la COP21 et pour mettre en valeur les données ouvertes relative au climat, à la transition énergétique et au développement durable, Etalab a publié sur sa data.gouv.fr une série d'articles "La COP21 en données" (voir les articles sur l'[énergie](https://www.data.gouv.fr/fr/posts/la-cop21-en-donnees-1-4-promouvoir-des-energies-et-des-processus-industriels-moins-polluants/), les [transports](https://www.data.gouv.fr/fr/posts/la-cop21-en-donnees-2-4-modes-doux-et-transports-partages-pour-une-mobilite-durable/), la [nature](https://www.data.gouv.fr/fr/posts/la-cop21-en-donnees-3-4-proteger-la-nature-et-la-biodiversite-dans-les-champs-et-les-forets/) et la [ville verte](https://www.data.gouv.fr/fr/posts/la-cop21-en-donnees-4-4-vers-une-ville-plus-verte-et-organisee-de-facon-circulaire/)).  
- Ouverture de nouveaux jeux de données via les hackathons et le projet [Green Tech Vertes](http://www.developpement-durable.gouv.fr/GreenTech-Vertes.html)
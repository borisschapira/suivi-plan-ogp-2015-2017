# 3. Recommandations du Comité d'experts indépendants

Cette partie sera complétée sur la base des retours apportés par le Comité d'experts indépendants ([IRM - Independent Reporting Mechanism](http://www.opengovpartnership.org/irm/about-irm)).
Les plans d'action des pays membres du [Partenariat pour un gouvernement ouvert](http://www.opengovpartnership.org/) sont en effet évalués tous les deux ans par un comité d'experts indépendants, nommés par le Partenariat. Concernant le Plan d'action national 2015-2017, deux rapports seront produits par ce comité : 
- Un premier rapport à mi-parcours, attendu d'ici fin septembre 2016
- Un rapport final prévu pour septembre 2017

Les résultats de ces rapports permettront à la France d'améliorer le processus de mise en oeuvre de son Plan d'action national.



# Engagement 6 : Faciliter l'accès aux données relatives aux obligations de transparence des responsables publics

## Institution porteuse : 
- Haute Autorité pour la transparence de la vie publique

## Enjeux : 
**Depuis les lois du 11 octobre 2013, la transparence de la vie publique connait une nouvelle impulsion**. Ces lois prévoient notamment que les 10 000 plus hauts responsables publics élus ou nommés doivent déclarer leur patrimoine et leurs intérêts à la **Haute Autorité pour la transparence de la vie publique (HATVP)**, chargée de les contrôler, en associant largement la société civile à ces contrôles.

Les déclarations de situation patrimoniale et d'intérêts des membres du gouvernement ainsi
que les déclarations d'intérêts des parlementaires nationaux et européens et des élus locaux
sont rendues publiques et diffusées sur le site internet de la HATVP. Selon le Conseil
constitutionnel, cette publication permet « à chaque citoyen de s'assurer par lui-même de la
mise en œuvre des garanties de probité et d'intégrité de ces élus, de prévention des conflits
d'intérêts et de lutte contre ceux-ci». Par ailleurs, il est également permis à un citoyen de
porter à la connaissance de la Haute Autorité des informations dont il dispose et qui ne
figureraient pas dans les déclarations publiées.

Les déclarations peuvent être transmises à la HATVP par voie papier, ou depuis le décret du 3
mars 2015, par télé service. Ce nouvel outil de déclaration en ligne permet d'améliorer
l'accessibilité des déclarations publiées, en évitant les problèmes d'interprétations liés à des
déclarations manuscrites. Cette dynamique doit ainsi être poursuivie en encourageant la
diffusion des données publiées dans un format ouvert et aisément exploitable.

## Description de l'engagement : 

- **Publier sous format ouvert et réutilisable les données publiques des déclarations de situation patrimoniale et d'intérêts soumises à publicité et effectuées par l'intermédiaire d'un télé-service** (déclarations de situation patrimoniale des membres du gouvernement et déclarations d'intérêts des membres du gouvernement, parlementaires, représentants français au Parlement européen et principaux élus locaux)
    * L'évolution de l'application pour déclarer en ligne (ADEL) rendra possible, courant 2016, la diffusion des informations dans un format réutilisable
    * L'action de sensibilisation, menée par la HATVP en faveur de la télé-déclaration, sera intensifiée auprès des personnes soumises aux obligations déclaratives, pour garantir un fort taux de télé déclaration et, partant, un volume important de données publiées en open data


[En savoir plus](http://gouvernement-ouvert.etalab.gouv.fr/content/fr/rendre-des-comptes/transparence-responsables-publics/engagement-6.html)
 
## Description des résultats :

**Actions** | **Résultats** | **Prochaines étapes** | **Statut**
--- | --- | --- | ---
Publier sous format ouvert et réutilisable les données publiques des déclarations de situation patrimoniale et d’intérêts soumises à publicité et effectuées par l’intermédiaire d’un télé-service. | L'action de sensibilisation en faveur d'une plus grande utilisation du téléservice (Application pour Déclarer en Ligne - ADEL V1) a été renforcée de manière significative. Le taux d'utilisation a fortement cru depuis le lancement - 20% en mars 2015 - pour s’établir, au premier trimestre 2016, à environ 60%. Le [décret n° 2016-570 du 11 mai 2016](https://www.legifrance.gouv.fr/eli/decret/2016/5/11/PRMX1607948D/jo) relatif à la transmission à la Haute Autorité pour la transparence de la vie publique des déclarations de situation patrimoniale et déclarations d'intérêts par l'intermédiaire d'un téléservice est paru au Journal officiel le 13 mai 2016 . Il donne suite à une proposition formulée dans le rapport d’activité 2015 de la Haute Autorité en consacrant le passage à la télédéclaration : à compter du 15 octobre 2016, tous les responsables publics devront utiliser le téléservice ADEL pour réaliser leurs déclarations de patrimoine et d’intérêts. Cette évolution réglementaire permettra de dématérialiser entièrement les déclarations. Elle est une condition essentielle à la publication en open data des données des déclarations pour lesquelles les lois ont prévu une publicité.| La [Haute Autorité](http://www.hatvp.fr/) a fait de l’open data une priorité. Tous les projets informatiques qu'elle mène en interne visent à réaliser cet objectif. Cela comprend la mise en place d'une seconde version de l'actuel téléservice (ADEL V2) afin de : le rendre pleinement accessible à tous, notamment aux personnes en situation de handicap visuel, moteur ou disposant de matériel informatique ancien (objectif 2016) et permettre l’anonymisation de certaines informations contenues dans les déclarations  (ex : adresse personnelle, nom du conjoint, localisation des biens immobiliers etc.) selon un processus informatique adapté au choix du format ouvert. Ces évolutions  permettront à la Haute Autorité d’être techniquement prête à partir de la fin du 1er trimestre 2017. Les déclarations reçues à l'issue des élections présidentielle et législatives de 2017 constitueront le 1er lot important de déclarations publiées en open data.  Cela vise les déclarations de patrimoine et d'intérêts des membres du Gouvernement ainsi que les déclarations d'intérêts et d'activités des 577 députés. Les élections sénatoriales qui se tiendront en septembre 2017 donneront également lieu à la publication, en open data, des données des déclarations d'intérêts des 170 sénateurs de la série 1. Au total, ce sont plus de 700 déclarations qui seront publiées en open data au cours de la période 2017-2018. | Partiel

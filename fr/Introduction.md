# 1. Introduction et contexte

## Le Plan d'action national de la France

[Le Plan d'action national 2015-2017 "Pour une action publique transparente et collaborative"](http://gouvernement-ouvert.etalab.gouv.fr/content/fr//), publié par la France le 16 juillet 2015 comporte 26 engagements reposant sur 4 axes :
- Rendre des comptes
- Consulter, concerter et coproduire l'action publique
- Partager des ressources numériques utiles à l'innovation économique et sociale
- Poursuivre l'ouverture de l'administration

La dernière partie du plan d'action propose 3 engagements appliquant les principes du gouvernement ouvert au climat et au développement durable, et ont été mis en oeuvre dans la perspective de la [COP21](http://www.cop21.gouv.fr/), organisée à Paris en décembre 2015.

## Le Plan d'action de l'Assemblée nationale
Parallèlement, et dans le respect du principe constitutionnel de séparation des pouvoirs, l'Assemblée nationale a rejoint la démarche du gouvernement ouvert en publiant son propre plan d'action ["Vers une Assemblée nationale du XXIème siècle"](http://www.opengovpartnership.org/sites/default/files/Plan%20d%E2%80%99action%20de%20l%E2%80%99Assembl%C3%A9e%20Nationale.pdf). Son évaluation est également indépendante du présent rapport d'autoévaluation.

## Les 26 engagements du Plan d'action national reflètent pleinement les quatre principes clés du gouvernement ouvert : 
- Il s'agit d'abord d'instiller, à chaque niveau de l'action publique et de la vie économique, davantage de **transparence**. L'ouverture des données relatives à l'activité financière des collectivités territoriales ([engagement 1](Engagement1.md)), à l'aide au développement ([engagement 3](Engagement3.md)), aux obligations des responsables publics ([engagement 6](Engagement6.md)), au climat et au développement durable ([engagement 25](Engagement25.md)) en sont des exemples forts.
- La décision publique s'ouvre à **la participation citoyenne**, par des actions de co-élaboration de la loi et d'association de la société civile à la vie publique (engagements [10](Engagement10.md), [11](Engagement11.md), [12](Engagement12.md), [13](Engagement13), [19](Engagement19.md), [24](Engagement24.md), [26](Engagement26.md)), d'ouverture des évaluations de politiques publiques ([engagement 4](Engagement4.md)), de médiation en matière de justice et sur le fonctionnement des institutions (engagements [14](Engagement14.md) et [5](Engagement5.md)). L'administration se modernise en s'ouvrant à d'autres profils et d'autres pratiques (engagements [20](Engagement20.md), [21](Engagement21.md), [22](Engagement22.md), [23](Engagement23)).
- La plus forte implication de la France dans des initiatives internationales liées à la transparence de la commande publique ([engagement 2](Engagement2.md)), des bénéficiaires effectifs des sociétés ([engagement 7](Engagement7.md)), des paiements et revenus des industries extractives ([engagement 8](Engagement8.md)), des négociations commerciales ([engagement 9](Engagement9.md)), l'ouverture des modèles de calcul et des simulateurs de l'Etat ([engagement 16](Engagement16.md)) participent d'une plus grande **redevabilité de l'action publique**.
- Le **numérique et l'innovation** sont au coeur de la démarche de gouvernement ouvert, grâce à une politique renforcée en matière d'ouverture et de circulation des données ([engagement 15](Engagement15.md)) et le partage des ressources numériques de l'Etat et la modernisation de la conception des services publics (engagements [17](Engagement17.md) et [18](Engagement18.md)).


## Ce rapport d'autoévaluation à mi-parcours marque la première année de mise en oeuvre du Plan d'action national : 

- Il rappelle la [méthode d'élaboration, de mise en oeuvre et de suivi du plan](processus-elaboration-mise-en-oeuvre.md), et présente [l'avancement des engagements à date](mise-en-oeuvre-engagements-pan.md), les [actions initiées par la France pour partager son expérience et ses ressources](Echange-pairs-apprentissage.md) auprès d'autres pays, et [les prochaines étapes](conclusion-autres-prochaines-etapes.md).
- Il sera enrichi en continu et ce jusqu'en juillet 2017, date à laquelle la France devra remettre un rapport d'autoévaluation final et son deuxième plan d'action.
- Une section ["Contribuer"](contribuer.md) est ouverte : vous pouvez y proposer des suggestions d'amélioration dans la méthode et les outils de suivi du plan d'action.

## Calendrier global pour la France dans le Partenariat pour un gouvernement ouvert


![ogp-timeline-france](images/ogp-timeline-france.PNG)



[En savoir plus : fiche pays de la France sur le site du PGO](http://www.opengovpartnership.org/country/france/)
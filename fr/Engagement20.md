# Engagement 20 : Diversifier le recrutement au sein des institutions publiques

## Institutions porteuses : 
- Ministère de la Fonction publique

## Enjeux : 
La fonction publique se doit d'être exemplaire en reflétant la société française qu'elle sert, avec sa diversité et ses évolutions.

## Description de l'engagement : 
- **Développer de nouvelles voies d’accès à la fonction publique, pour mieux l’ouvrir sur la société**
    - Insérer dans le projet de loi relatif à la déontologie et aux droits et obligations des fonctionnaires un dispositif permettant de renouveler les voies d’accès à la fonction publique et de les ouvrir à des profils diversifiés
    - Rénover le dispositif de classe préparatoire à l’accès aux concours de catégorie A des trois fonctions publiques, en augmentant notamment, dès 2015, de 25% le nombre de places offertes dans les classes préparatoires intégrées destinées à la préparation des concours de la fonction publique. L’objectif pour 2016 est de doubler le nombre des élèves pour atteindre 1 000 places
    - Développer l’apprentissage dans la fonction publique en multipliant par 10 le nombre d’apprentis dans la fonction publique de l’État,  pour atteindre un objectif de 4 000 à la rentrée 2016 et 10 000 à la rentrée 2017
- **Traiter les biais discriminatoires à l’entrée dans la fonction publique**
    - Lancer, à la demande du Premier ministre, une mission d’expertise sur les questions de discrimination 
    - Modifier les textes portant sur la composition des jurys et comités de sélection de chaque ministère pour l’ouvrir à au moins un membre extérieur à l’administration qui recrute
    - Généraliser les formations à la prévention des discriminations pour tous les futurs membres de jurys et comités de sélection
    - Généraliser les procédures de labellisation des directions des ressources humaines afin d’évaluer l’ensemble de leurs procédures au regard des risques discriminatoires qu’elles peuvent comporter

[En savoir plus](http://gouvernement-ouvert.etalab.gouv.fr/content/fr/ouvrir-l-administration/acces-fonction-publique/engagement-20.html)
 
## Description des résultats :

### Développer de nouvelles voies d’accès à la fonction publique, pour mieux l’ouvrir sur la société

**Actions** | **Résultats** | **Prochaines étapes** | **Statut**
--- | --- | --- | ---
Insérer dans le projet de loi relatif à la déontologie et aux droits et obligations des fonctionnaires un dispositif permettant de renouveler les voies d’accès à la fonction publique et de les ouvrir à des profils diversifiés. | Cette proposition a été rejetée dans le [projet de loi relatif à la déontologie et aux droits et obligations des fonctionnaires]( https://www.legifrance.gouv.fr/affichLoiPreparation.do?idDocument=JORFDOLE000027721584&type=general), après un avis négatif du Conseil d’Etat le 2 juillet 2015. Désormais, les mesures retenues dépassent l'écueil juridique relevé par le Conseil d'Etat et passent par le [projet de loi « Egalité et citoyenneté »]( https://www.legifrance.gouv.fr/affichLoiPreparation.do?idDocument=JORFDOLE000032396041&type=general&typeLoi=proj&legislature=14). | Suivre le projet de loi « Egalité et citoyenneté ». | Partiel
Rénover le dispositif de classe préparatoire à l’accès aux concours de catégorie A des trois fonctions publiques, en augmentant notamment, dès 2015, de 25% le nombre de places offertes dans les classes préparatoires intégrées (CPI) destinées à la préparation des concours de la fonction publique. L’objectif pour 2016 est de doubler le nombre des élèves pour atteindre 1 000 places | L'objectif de doubler le nombre de places en CPI pour atteindre les 1000 en 2016 est atteint : +5 places à l'Ecole nationale d’administration (ENA) et + 86 places en Institut régional d’administration (IRA). [Le nombre de places dans les classes préparatoires de 2010 à 2014 est disponible sur data.gouv.fr](https://www.data.gouv.fr/fr/datasets/les-classes-preparatoires-aux-grandes-ecoles-cpge-00000000/). |   | Complet
Développer l’apprentissage dans la fonction publique en multipliant par 10 le nombre d’apprentis dans la fonction publique de l’État, pour atteindre un objectif de 4 000 à la rentrée 2016 et 10 000 à la rentrée 2017. | L’objectif de renforcement du nombre d'apprentis a été atteint pour 2016. | Poursuivre en 2017. Suivre la politique de renforcement de l'apprentissage dans la fonction publique. | Substantiel
Lancer, à la demande du Premier ministre, une mission d’expertise sur les questions de discrimination. Modifier les textes portant sur la composition des jurys et comités de sélection de chaque ministère pour l’ouvrir à au moins un membre extérieur à l’administration qui recrute. Généraliser les formations à la prévention des discriminations pour tous les futurs membres de jurys et comités de sélection. Généraliser les procédures de labellisation des directions des ressources humaines afin d’évaluer l’ensemble de leurs procédures au regard des risques discriminatoires qu’elles peuvent comporter. | Une [mission visant à coordonner l’élaboration et la mise en œuvre des programmes d’action des écoles de service public](http://www.fonction-publique.gouv.fr/ecoles-de-service-public-et-la-diversite-presentation-de-la-mission-rousselle) a été confiée à Olivier Rousselle le 24 février 2016. Dans ce cadre, la Direction générale de l’administration et de la fonction publique  (DGAFP) a invité les ministères à une réunion le 13 avril 2016 afin de préparer trois groupes de travail sur le sujet. Par ailleurs, une mission d’évaluation des voies d’accès à la fonction publique avait été confiée à Yannick L’Horty le 23 juin 2015 et ses conclusions sont attendues en juin 2016. | Modifier les textes portant sur la composition des jurys : une circulaire a été envoyée aux Ministères pour modifier les textes. Généraliser les formations à la prévention des discriminations et les labellisations des directions des ressources humaines (prévu en septembre 2016 dans le cadre des mesures des Comités interministériels à l'Egalité et à la Citoyenneté). | Substantiel


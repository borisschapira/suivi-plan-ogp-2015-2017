# Engagement 8 : Renforcer la transparence des paiements et revenus issus des industries extractives

**Institutions porteuses** :
- Ministère des Affaires étrangères et du Développement international
- Ministère de l'Environnement, de l'Energie et de la Mer
- Ministère des Finances et des Comptes publics
- Ministère de l'Économie, de l'Industrie et du Numérique

### Enjeux

**La transparence sur les industries extractives vise à promouvoir une plus grande
responsabilité sociale des entreprises et une meilleure gouvernance publique**, ainsi qu'à
accroître la confiance des investisseurs et du public dans le secteur minier.

Elle répond également au devoir d'exemplarité que la France souhaite exercer vis-à-vis des
pays en développement et des pays émergents, en renforçant les normes qui contribuent à
mettre les entreprises internationales sur un pied d'égalité. Elle accompagne la volonté
politique de développer une activité minière responsable en Guyane et de promouvoir le
domaine minier métropolitain.

## Description de l'engagement : 
**Adhérer à l'Initiative pour la Transparence dans les Industries extractives (ITIE) et travailler sur l'accessibilité des données ouvertes dans le cadre de l'ITIE et des déclarations des entreprises au titre du chapitre 10 de la directive comptable européenne :**
* Été 2015 : désignation du haut représentant français pour l'ITIE et mise en place d'une équipe projet dotée des moyens humains et financiers nécessaires pour préparer la candidature de la France à l'ITIE
* Septembre 2015 : constitution d'un comité national tripartite pour l'ITIE
* Mars 2016 : première déclaration des entreprises au titre du chapitre 10 de la directive comptable
* Avant décembre 2016 : présentation de la candidature de la France à l'ITIE
* 1er semestre 2017 : la France devient « pays candidat » de l'ITIE

[En savoir plus](http://gouvernement-ouvert.etalab.gouv.fr/content/fr/rendre-des-comptes/transparence-vie-economique/engagement-8.html)
 
## Description des résultats :

**Actions** | **Résultats** | **Prochaines étapes** | **Statut**
--- | --- | --- | ---
Adhérer à l’[Initiative pour la Transparence dans les Industries extractives]( https://eiti.org/fr/itie) (ITIE) et travailler sur l’accessibilité des données ouvertes dans le cadre de l’ITIE et des déclarations des entreprises au titre du chapitre 10 de la directive comptable européenne. | Le processus d'adhésion a été officiellement lancé en février 2016 ([communiqué de presse](http://proxy-pubminefi.diffusion.finances.gouv.fr/pub/document/18/20536.pdf)). Un groupe de travail, présidé par Isabelle WALLARD (CGIET) a été constitué, afin de préparer l'adhésion et définir les modalités (périmètre financier, géographique, constitution du comité tripartite). Ce communiqué de presse indique que la France entend présenter sa candidature à l’ITIE d’ici la fin 2017. 2 réunions inter administration se sont tenues (mars et avril 2016), mobilisant la DGALN, la DGEC, la DGT, la DGFIP, la DGOM, et la DGM. Le travail interministériel se concentre sur la définition du périmètre financier. Les entreprises extractives ont été sensibilisées et soutiennent la démarche. Une question se pose quant à l’intégration des territoires d’outre-mer à cette initiative, la Guyane et la Nouvelle Calédonie notamment. Le sujet du secret fiscal, qui devra être partiellement levé pour permettre la publication d’informations financières dans les rapports annuels attendus, fait également l’objet de travaux internes. Une table ronde avec la société civile (ONG, syndicats, journalistes, chercheurs) a été organisée le 3 juin. L'objectif est de consolider un projet de comité tripartite, nécessaire à la préparation de candidature.  Le comité tripartite, qui comprendra des représentants de l’administration, des entreprises et de la société civile, sera chargé de préparer cette candidature à l’ITIE en définissant notamment les objectifs poursuivis par la France, et devra arbitrer les questions techniques, telles que la définition du périmètre des matières couvertes ou des périmètres géographique et financier retenus. La faible mobilisation de la société civile sur les enjeux de transparence des industries extractives en France, et la faiblesse des moyens humains et financiers mis à disposition par l’administration pour soutenir cet exercice, risquent de ralentir le processus. |  Une réunion officielle de lancement, présidée par le ministre de l’économie, de l’industrie et du numérique, est prévue pour le 30 juin 2016 afin de consolider un projet de comité tripartite. Un comité tripartite rassemblant les administrations, les entreprises et la société civile, devra être mis en place d’ici la mi-juillet afin d’engager la préparation du dossier de candidature. Il conviendra d’élargir les contacts auprès des représentants de la société civile. | Substantiel
Action supplémentaire   | Une réunion a eu lieu entre Etalab et Isabelle Wallard pour initier les réflexions sur l'ouverture des données des entreprises du secteur extractif dans le cadre de l'adhésion à l'ITIE. | Travailler à ouvrir les données contenues dans les rapports annuels des entreprises extractives françaises.| Partiel

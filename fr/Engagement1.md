# Engagement 1 : Permettre à tous de consulter, de comprendre et de réutiliser les données financières et les décisions des collectivités territoriales

## Institutions porteuses : 
- Ministère de l'Intérieur
- Ministère des Finances et des Comptes publics
- Ministère de l’Aménagement du territoire, de la Ruralité et des Collectivités territoriales
- Cour des comptes
- Ministère de l’Environnement, de l’Énergie et de la Mer

## Enjeux : 
Afin de mieux satisfaire l'attente légitime des citoyens et compte tenu de la part importante que les collectivités territoriales prennent à la dépense publique, leur transparence financière doit être renforcée.

## Description de l'engagement : 
- **Publier en open data les données des collectivités territoriales**
    - Permettre aux citoyens de mieux saisir les enjeux financiers des collectivités territoriales
    - Mettre à disposition régulièrement les données financières des juridictions financières
    - Renforcer l'open data des collectivités territoriales : inscrire dans la loi l'obligation de publier les informations publiques des collectivités de plus de 3500 habitants (y compris communes et EPCI)
- **Publier en ligne les délibérations et comptes rendus des conseils municipaux**
    - Publier sous forme électronique, et mettre à disposition de manière permanente et gratuite, en plus d’une version papier, le recueil des actes administratifs des délibérations et arrêtés municipaux des collectivités
    - Afficher dans un délai d’une semaine suivant le conseil municipal et mettre en ligne sur le site internet de la commune, quand ils existent, les comptes rendus des conseils municipaux, pendant six ans minimum
- **Publier en open data des données relatives aux permis de construire**
    - Engager un groupe de travail avec les parties prenantes pour faire organiser progressivement la disponibilité en open data des données relatives aux permis de construire d’ici 2017

[En savoir plus](http://gouvernement-ouvert.etalab.gouv.fr/content/fr/rendre-des-comptes/transparence-depense-et-comptes-publics/engagement-1.html)

## Description des résultats :

**Actions** | **Résultats** | **Prochaines étapes** | **Statut**
--- | --- | --- | ---
Publier en open data la dotation globale de fonctionnement, contribution financière de l'Etat aux collectivités territoriales. | Les [données des dotations](http://www.dotations-dgcl.interieur.gouv.fr) sont disponibles sur le site du ministère de l'Intérieur. | Référencer les données des dotations sur data.gouv.fr. Mettre en ligne les critères physiques et financiers servant de bases de calcul à la dotation globale de fonctionnemen. | Substantiel
Publier en open data sur data.gouv.fr l'intégralité des balances comptables, à compter de l'exercice 2013, des collectivités locales et des groupements à fiscalité propre. | Les [balances comptables](https://www.data.gouv.fr/fr/datasets/?q=balances+comptables&organization=534fff8ea3a7292c64a77f02) sont disponibles à partir de 2013 sur data.gouv.fr. | Obtenir un historique des balances comptables. Mettre à jour les données tous les ans. | Complet
Rendre obligatoire pour les exécutifs locaux et les présidents d'EPCI  de présenter un rapport devant l'assemblée délibérante (conseil municipal, départemental ou régional) sur les suites données aux observations de la chambre régionale des comptes (CRC). | Cet engagement a été inscrit dans la [loi NOTRe](https://www.legifrance.gouv.fr/affichLoiPubliee.do;jsessionid=A4F27320020462891B15569F43C59812.tpdila12v_1?idDocument=JORFDOLE000029101338&type=contenu&id=2&typeLoi=&legislature=14) (nouvel article L 243-7). | Suivre les décrets d'application de la loi NOTRe. | Substantiel
Mettre à disposition régulièrement les données financières des juridictions financières : les données fondant les travaux concernant les finances locales et certaines données d'activité des juridictions financières (notamment la mise à jour de la liste des publications de la Cour des comptes et des moyens des juridictions financières). | Les [données des rapports](https://www.data.gouv.fr/fr/organizations/cour-des-comptes/#datasets) de la Cour des comptes sur les finances publiques locales sont disponibles sur data.gouv.fr. De nouveaux jeux de données ont été publié le 27 mai 2016 à l'occasion d'une ["DataSession"]( https://www.etalab.gouv.fr/datasession-a-la-cour-des-comptes-une-premiere-brique-vers-louverture-des-decisions-de-justice) de la Cour des comptes : les jugements anonymisés des chambres régionales et territoirales des comptes (2016), et les rapports d'observations définitives des chambres régionales et territoirales des comptes (2013, 2014 et 2015). [Voir toutes les données de la Cour des comptes](https://www.data.gouv.fr/fr/organizations/cour-des-comptes/) | Publier les données d'activité des juridictions financières, notamment la mise à jour de la liste des publications de la Cour et des moyens des juridictions financières. Donner davantage de profondeur historique aux jeux de données publiés. | Complet
Renforcer l’open data des collectivités territoriales :  inscrire dans la loi l’obligation de publier les informations publiques des collectivités de plus de 3 500 habitants (y compris communes et EPCI). | La [loi NOTRe](https://www.legifrance.gouv.fr/affichLoiPubliee.do;jsessionid=A4F27320020462891B15569F43C59812.tpdila12v_1?idDocument=JORFDOLE000029101338&type=contenu&id=2&typeLoi=&legislature=14) (titre IV) établit l'obligation de publier les informations publiques des collectivités de plus de 3 500 habitants (y compris communes et EPCI). | Suivre les décrets d'application de la loi NOTRe. Accompagner les collectivités territoriales dans l'ouverture. | Substantiel
Publier sous forme électronique, et mettre à disposition de manière permanente et gratuite, en plus d’une version papier, le recueil des actes administratifs des délibérations et arrêtés municipaux des collectivités. Afficher dans un délai d’une semaine suivant le conseil municipal et mettre en ligne sur le site internet de la commune, quand ils existent, les comptes rendus des conseils municipaux, pendant six ans minimum. | La [loi NOTRe](https://www.legifrance.gouv.fr/affichLoiPubliee.do;jsessionid=A4F27320020462891B15569F43C59812.tpdila12v_1?idDocument=JORFDOLE000029101338&type=contenu&id=2&typeLoi=&legislature=14) (titre IV) établit cette obligation. | Suivre les décrets d'application de la loi NOTRe. | Partiel
Engager un groupe de travail avec les parties prenantes pour faire organiser progressivement la disponibilité en open data des données relatives aux permis de construire d’ici 2017. | Un groupe de travail sur l'anonymisation a été instauré entre Etalab, l'Administrateur général des données et le Commissariat général au développement durable. Une saisine CNIL est en cours de traitement. | Suivre l'avis de la CNIL et les avancées du groupe de travail. | Partiel


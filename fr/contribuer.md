# 7. Contribuer

Dans cette section : 
- vous pouvez formuler directement via le forum des suggestions sur la méthodologie d'élaboration et de suivi du plan d'action (outils, calendrier, formats, indicateurs, etc.)
- les contributions écrites envoyées à l'adresse <mailto:gouvernement-ouvert@etalab.gouv.fr> seront progressivement mises en ligne sur cet espace
- des informations sur les prochains événements présentiels seront également publiées



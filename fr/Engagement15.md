# Engagement 15 : Renforcer la politique d'ouverture et de circulation des données

## Institutions porteuses : 
- Secrétariat d'État chargé de la Réforme de l'État et de la Simplification
- Secrétariat d'État chargé du Numérique

## Enjeux : 
La France, son gouvernement et ses collectivités territoriales se sont engagés avec force dans l'ouverture et le partage des données publiques. Cette politique essentielle est considérée à la fois comme un ressort de vitalité démocratique, une stratégie d'aide à l'innovation économique et sociale, et un levier pour la modernisation de l'action publique.

## Description de l'engagement : 
- **Poursuivre l'ouverture des données à fort impact économique et social, et notamment des « données-pivot »**
- **Renforcer l'open data des collectivités territoriales : inscrire dans la loi l'obligation de publier les informations publiques des collectivités de plus de 3500 habitants (y compris communes et EPCI)**
- **Inscrire dans la loi les principes d'ouverture par défaut des données publiques (avec fermeture par exception) et de leur réutilisation libre et gratuite**
- **Approfondir l'étude d'opportunité sur l'ouverture des « données d'intérêt général »** 

[En savoir plus](http://gouvernement-ouvert.etalab.gouv.fr/content/fr/partager-des-ressources/engagement-15.html)
 
## Description des résultats :

**Actions** | **Résultats** | **Prochaines étapes** | **Statut**
--- | --- | --- | ---
Poursuivre l'ouverture des données à fort impact économique et social, et notamment des « données-pivot ». | L'article 4 du [projet de loi pour une République numérique](https://www.legifrance.gouv.fr/affichLoiPreparation.do?idDocument=JORFDOLE000031589829&type=general&typeLoi=proj&legislature=14) instaure la notion de données à intérêt économique et social. Dans ce cadre, [l'ouverture de la base SIRENE est prévue au 1er janvier 2017](https://www.etalab.gouv.fr/louverture-du-repertoire-sirene-par-linsee-au-1er-janvier-2017-une-avancee-majeure-pour-lopen-data). Cette ouverture a été annoncée à l'occasion de la conférence-débat ["#DataDay : développer l'économie de la donnée"](http://www.economie.gouv.fr/dataday-developper-economie-de-la-donnee) organisée le 12 janvier 2016 par le secrétariat d'Etat chargé du Numérique. Des travaux sont en cours afin de définir les données pivots (jeux de données de qualité qui serviront de référence. En parallèle, un travail a été fait sur la montée en qualité des données disponibles sur la plateforme data.gouv.fr (mise en place d'un indicateur de qualité pour les producteurs de données et relance automatique lorsqu'une mise à jour des données est nécessaire). | Poursuivre les travaux engagés avec la FING dans le cadre de la campange [Infolab](http://fing.org/?-Infolab-) afin d'améliorer la qualité des données à fort impact économique et social. | Substantiel  
Renforcer l'open data des collectivités territoriales : inscrire dans la loi l'obligation de publier les informations publiques des collectivités de plus de 3500 habitants (y compris communes et EPCI). | Cet engagement a été inscrit dans la [loi du 7 août 2015 portant nouvelle organisation territoriale de la République](https://www.legifrance.gouv.fr/affichLoiPubliee.do;jsessionid=A4F27320020462891B15569F43C59812.tpdila12v_1?idDocument=JORFDOLE000029101338&type=contenu&id=2&typeLoi=&legislature=14) (article 30). | Suivre les décrets d'application de la loi et accompagner les collectivités de plus de 3 500 habitants dans l'ouverture effective de leurs données. | Substantiel 
Inscrire dans la loi les principes d'ouverture par défaut des données publiques (avec fermeture par exception) et de leur réutilisation libre et gratuite. | Le [projet de loi pour une République numérique](https://www.legifrance.gouv.fr/affichLoiPreparation.do?idDocument=JORFDOLE000031589829&type=general&typeLoi=proj&legislature=14) pose le principe d'une ouverture par défaut des données publiques. La [loi du 28 décembre 2015 relative à la gratuité et aux modalités de la réutilisation des informations du secteur public](https://www.legifrance.gouv.fr/affichTexte.do;jsessionid=7E3851E32FA0CB87DD653ABFF4A797F0.tpdila20v_3?cidTexte=JORFTEXT000031701525&categorieLien=id) pose le principe de la gratuité de la réutilisation des informations publiques. | Suivre l'examen du projet de loi pour une République numérique en Commission mixte paritaire. | Substantiel
Approfondir l'étude d'opportunité sur l'ouverture des « données d'intérêt général ». | Jugée opportune, l'ouverture des données d'intérêt général a été inscrite dans le [projet de loi pour une République numérique](https://www.legifrance.gouv.fr/affichLoiPreparation.do?idDocument=JORFDOLE000031589829&type=general&typeLoi=proj&legislature=14) pour les contrats de concessions et les subventions. Une nouvelle mission est en cours sur les données d'intérêt général. | Suivre l'examen du projet de loi pour une République numérique en Commission mixte paritaire et les conclusions de la mission en cours. | Substantiel

## Une belle histoire : 

La démarche d'ouverture des données du ministère de l'Education nationale, de l'Enseignement supérieur et de la Recherche va connaître un nouveau développement à la rentrée scolaire 2016 : 
- Le ministère lancera en septembre sa plateforme open data sur le périmètre éducation nationale : [data.education.fr](http://data.education.fr/). Elle s'accompagnera de l'ouverture de nouveaux jeux de données (rythmes scolaires, etc.). 
- Le ministère finalise son projet PIA (Programme d'investissements d'avenir) sur les données de l'éducation prioritaires. Un recensement des données prioritaires à ouvrir par direction est en cours et une trajectoire d'ouverture sera définie en juillet 2016 par le comité de pilotage ministériel open data. Un travail en lien avec les académies et les opérateurs est également en cours afin de construire une véritable stratégie ministérielle concertée et partagée.
- Un "Educathon" (hackathon sur les données de l'éducation) est en cours d'organisation.  

Sur le périmètre recherche, le ministère travaille également sur des outils open source pour exploiter les données ouvertes : 
- Concevoir un outil industriel et générique de gestion et d'exposition des données dans une perspective d'intensification de l'usage et de l'ouverture des données ("DataESR")
- Proposer un outil, accessible à tous, de profilage des laboratoires publics et des entreprises dans leur engagement en recherche et innovation dont l'ensemble des données sous jacentes seront exposées sous licence ouverte ("ScanR") 

## Ils en parlent : 
- Silicon, ["Open Data : Ouverture des données SIRENE au début 2017"](http://www.silicon.fr/open-data-ouverture-des-donnees-sirene-au-debut-2017-135562.html#Ze1bLh8yOMVYGcfD.99), 12 janvier 2016
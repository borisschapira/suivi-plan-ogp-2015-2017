# Engagement 7 : Identifier les bénéficiaires effectifs des entités juridiques enregistrées en France pour lutter efficacement contre le blanchiment


## Institutions porteuses : 
- Ministère de la Justice
- Ministère des Finances et des Comptes publics
- Ministère de l'Économie, de l'Industrie et du Numérique


## Enjeux : 
**La connaissance des clients et bénéficiaires des activités financières est l'un des piliers de la
lutte anti-blanchiment, de la lutte contre la corruption et de l'évasion fiscale** et permet de
**déceler des opérations atypiques pouvant être liées à des transactions délictueuses.**

Le bénéficiaire effectifs d'une société, tel que défini à l'article L561-2-2 du Code monétaire et
Financier est « _la personne physique qui contrôle, directement ou indirectement, le client ou
celle pour laquelle une transaction est exécutée ou une activité réalisée_ ». Son identification,
prévue en particulier à l'article L561- 5 du même code, permet de renforcer la transparence
globale des sociétés-écran et des trusts, et de lutter contre le blanchiment de capitaux, la
corruption et l'évasion fiscale.


## Description de l'engagement : 
**Utiliser un registre centralisé, abondé de données variées, incluant les données du
registre français centralisé pour les entreprises (le Registre du Commerce et des Sociétés,
RCS), de manière à assurer et à fournir un accès largement ouvert à des informations
utiles, exactes et à jour sur les bénéficiaires effectifs des sociétés et autres entités
juridiques.**

[En savoir plus](http://gouvernement-ouvert.etalab.gouv.fr/content/fr/rendre-des-comptes/transparence-vie-economique/index.html)
 
## Description des résultats :

**Actions** | **Résultats** | **Prochaines étapes** | **Statut**
--- | --- | --- | ---
Utiliser un registre centralisé, abondé de données variées, incluant les données du registre français centralisé pour les entreprises (le Registre du Commerce et des Sociétés, RCS). |  A l'occasion du Sommet Anti-Corruption tenu à Londres le 12 mai 2016, la France a pris plusieurs engagements sur les registres des bénéficiaires effectifs. D'ici le 30 juin, le registre public des bénéficiaires effectifs des trusts sera constitué, il permettra de déterminer l'identité des personnes bénéficiaires de ces 16000 entités. Un registre public des bénéficiaires effectifs des sociétés sera constitué dans des modalités prochainement définies".  [Lien vers les engagements français](https://www.gov.uk/government/uploads/system/uploads/attachment_data/file/522751/France.pdf). **Sur le registre des bénéficiaires effectifs des sociétés** :  Des échanges ont eu lieu entre les ministères économiques et financiers et le ministère de la Justice. Le mardi 10 mai  2016, le cabinet du Premier ministre a arbitré  pour travailler sur le RCS afin qu’il soit le registre des bénéficiaires effectifs  des sociétés commerciales avec accès public. Il reviendra aux entreprises de collecter l’information en interne, de la tenir à jour et de la déclarer au RCS. La DG Trésor travaille actuellement en lien avec la Chancellerie sur la transposition de cet article en droit français. Concernant les sociétés, le MFCP  est favorable à la mise en place d’un registre (registre national du commerce et des sociétés électronique, tenu à l’INPI) qui sera consultable par le public. Toutefois, certaines informations concernant les bénéficiaires effectifs des sociétés n’ont pas vocation à être rendues publiques (ex : adresse), ce qui est techniquement possible, toutes les informations qui sont centralisées par le RNCS ne sont pas publiques. Des discussions sont en cours avec la Chancellerie sur ce point. **Sur le registre des bénéficiaires effectifs des trusts** :  par ailleurs, le registre des bénéficiaires effectifs des trusts, géré par la DGFIP, est en cours de création (le décret est en cours d’examen au Conseil d’Etat) ; il est public : voir [ici](https://www.legifrance.gouv.fr/eli/decret/2016/5/10/FCPE1414439D/jo). | Suivre  les travaux sur le registre des bénéficiaires effectifs des sociétés. Pour cela, définir  : 1/  les modalités de collecte par les entreprises des informations sur les bénéficiaires effectifs ; 2/  les mesures de sanctions ou de dissuasions pour assurer la fiabilité des informations collectées et déclarées par les entreprises ; 3/  les informations sur les bénéficiaires effectifs en open data. Suivre la transposition de la directive européenne dite "quatrième directive anti-blanchiment" publiée le 5 juin 2015. Suivre les suites du Sommet anticorruption de Londres le 12 mai 2016. Travailler à une articulation avec : le projet de loi Sapin II, la définition du service public de la donnée, l'ouverture de la base SIRENE le 1er janvier 2017. | Partiel
    

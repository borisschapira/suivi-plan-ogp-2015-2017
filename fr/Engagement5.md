# Engagement 5 : Impliquer davantage les citoyens dans les travaux menés par la Cour des comptes

## Institution porteuse : 
- Cour des comptes

## Enjeux : 
Chargée par la Constitution de porter une appréciation libre, indépendante et collégiale sur l'usage qui est fait des deniers publics, la Cour des comptes est l'une des grandes institutions de la République. Juridiction indépendante, elle se situe à équidistance du Parlement et du gouvernement, auxquels elle porte assistance. Elle joue un rôle essentiel au fonctionnement de notre démocratie et à la modernisation de l'action publique.

L'approfondissement de sa démarche d'ouverture des données publiques, l'encouragement de l'analyse fondée sur la donnée et la plus grande implication des citoyens sur les travaux menés par la Cour des comptes participent à l'engagement de l'institution dans la démarche française de gouvernement ouvert.

## Description de l'engagement : 
- **Diffuser en open data certaines données collectées lors de contrôles et d'évaluations, ainsi que des données propres aux juridictions financières**. La Cour des comptes s'engage à mettre à disposition régulièrement les jeux de données suivants :
    * Les données budgétaires fondant ou ayant fondé l'analyse de l'exécution du budget de l'État 
    * À chaque fois que cela sera possible, les données fondant ou ayant fondé les enquêtes thématiques de la Cour 
    * Les données fondant ou ayant fondé les travaux concernant les finances locales
    * Certaines données d'activité des juridictions financières, notamment la mise à jour de la liste des publications de la Cour et des moyens des juridictions financières

La Cour des comptes étudiera également, en lien avec les services du Premier ministre, la mise en place d'un portail de données « data.ccomptes.fr », répertorié sur data.gouv.fr, afin de systématiser une stratégie de gestion des données et la démarche d'ouverture des informations publiques.

- **Intéresser davantage le citoyen aux travaux de la Cour des comptes**. Cette implication pourrait prendre plusieurs formes :
    * une enquête d'opinion afin de mieux identifier les attentes du citoyen vis-à-vis des travaux de la Cour, sur le fond comme sur la forme
    * un espace contributif permettant une remontée des préoccupations des citoyens

[En savoir plus](http://gouvernement-ouvert.etalab.gouv.fr/content/fr/rendre-des-comptes/evaluation-publique/engagement-5.html)


## Description des résultats :

**Actions** | **Résultats** | **Prochaines étapes** | **Statut**
--- | --- | --- | ---
Diffuser en open data certaines données collectées lors de contrôles et d'évaluations, ainsi que des données propres aux juridictions financières. |  La Cour des Comptes a publié [18 jeux de données sur data.gouv.fr](https://www.data.gouv.fr/fr/organizations/cour-des-comptes/#datasets) à la sortie de ses rapports publics thématiques. De nouveaux jeux de données ont été publié le 27 mai 2016 à l'occasion de la #DataSession de la Cour des comptes : les jugements anonymisés des chambres régionales et territoirales des comptes (2016), et les rapports d'observations définitives des chambres régionales et territoirales des comptes (2013, 2014 et 2015). [Voir toutes les données de la Cour des comptes](https://www.data.gouv.fr/fr/organizations/cour-des-comptes/) | Suivre la mise à disposition régulière de nouveaux jeux de données et leur mise à jour par la Cour des comptes. | Complet
Intéresser davantage le citoyen aux travaux de la Cour des comptes | [La Cour des comptes a organisé les 27 et 28 mai 2016 une "DataSession"]( https://www.etalab.gouv.fr/datasession-a-la-cour-des-comptes-une-premiere-brique-vers-louverture-des-decisions-de-justice), hackathon officialisant la publication  en open data de nouveaux jeux de données (rapports publiés, jurisprudence, tableaux de chiffres dans les rapports, liste des organismes soumis au contrôle de la Cour des comptes). Des magistrats et rapporteurs de la Cour ont accompagné les porteurs de projets. Un [espace contributif]( https://forum.etalab.gouv.fr/c/agenda/datasession-a-la-cour-des-comptes-27-28-mai-2016) a été mis à disposition sur le forum Etalab en amont de l'événement et une première rencontre a été organisée avec les participants afin d'échanger sur le rôle de la Cour et les projets numériques qu'elle pourrait porter. Ce hackathon pourra aussi être l'occasion de spécifier les besoins techniques et non techniques pour la plateforme data.ccomptes.fr. | Suivre les résultats de la DataSession des 27 et 28 mai et les projets portés par la Cour. Continuer le travail consistant à mieux impliquer les citoyens dans le travail de la Cour, notamment à l’occasion des « journées du patrimoine » de septembre, conçues à la Cour des comptes comme des « journées citoyennes ». | Substantiel
